﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamDemo.Data;
using ExamDemo.Models;
using ExamDemo.ParamModel;
using Newtonsoft.Json;

namespace MVCDemo.Controllers
{
    public class UsersController : Controller
    {
        private ExamDemoDb db = new ExamDemoDb();

        // GET: Users
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Username,Password,CreatedAt,UpdatedAt,Version,Remarks")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(users);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(users);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Username,Password,CreatedAt,UpdatedAt,Version,Remarks")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(users);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Users users = db.Users.Find(id);
            db.Users.Remove(users);
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        public ActionResult Login()
        {
            var tmpUser = Session["username"] == null ? "" : Session["username"].ToString();

            if (tmpUser.Length > 0)
            {
                Response.Redirect("/");
            }


            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public string LoginDone(LoginModel loginModel)
        {
            dynamic result;

            var username = loginModel.Username.Trim();
            var password = loginModel.Password.Trim();

            if (username.Length > 0 && password.Length > 0)
            {
                var user = db.Users.Where(x => x.Username.Equals(username) && x.Password.Equals(password)).FirstOrDefault();

                // 如果查收到相应用户，是为登录成功
                if (user != null)
                {
                    // 登录成功，使用session保存登录信息
                    Session["id"] = user.Id;
                    Session["username"] = user.Username;
                    Session.Timeout = 80;

                    result = new
                    {
                        code = 200,
                        msg = "登录成功!!!"
                    };
                }
                else// 否则用户名或密码错误
                {
                    result = new
                    {
                        code = 1000,
                        msg = "用户名或密码错误"
                    };
                }
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名或密码不能为空，请重试"
                };
            }


            return JsonConvert.SerializeObject(result);
        }

        [HttpPost]
        public string RegisterDone(RegisterModel registerModel)
        {
            dynamic result;

            var username = registerModel.Username.Trim();
            var password = registerModel.Password.Trim();
            var confirmPassword = registerModel.ConfirmPassword.Trim();


            // 如果用户名、密码不为空，且两次密码一致，则继续进入逻辑
            if (username.Length > 0 && password.Length > 0 && password == confirmPassword)
            {
                var user = db.Users.Where(x => x.Username == username).FirstOrDefault();

                // 如果能找到同名的注册用户，则仍然返回提示信息
                if (user != null)
                {
                    result = new
                    {
                        code = 1000,
                        msg = "当前用户已经注册，请重试"
                    };
                }
                else
                {
                    db.Users.Add(new Users
                    {
                        Username = username,
                        Password = password
                    });

                    db.SaveChanges();

                    result = new
                    {
                        code = 200,
                        msg = "注册成功！"
                    };
                }
            }
            else// 否则则返回提示信息
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名或密码不能为空，且两次密码应当一致，请核对后重试"
                };
            }

            return JsonConvert.SerializeObject(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}