﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ExamDemo.Models;


namespace ExamDemo.Data
{
    public class DataInitlizer : DropCreateDatabaseIfModelChanges<ExamDemoDb>

    {
        protected override void Seed(ExamDemoDb db)
        {
            db.Users.AddRange(new List<Users>
            {
        new Users
        {
            Username ="小白",
            Password="1"
        },new Users{
            Username ="小绿",
            Password="1"
        },new Users{
            Username ="小黑",
            Password="1"
        },new Users{
            Username ="小黄",
            Password="1"
        },
        });
            db.Messages.AddRange(new List<Messages>
            {
                new Messages
                {
                    FromUserId=1,
                    Content="床前明月光"
                },new Messages
                {
                     FromUserId=2,
                    Content="疑是地上霜"
                },new Messages
                {
                     FromUserId=3,
                    Content="举头望明月"
                },new Messages
                {
                     FromUserId=4,
                    Content="低头思故乡"
                }

            });
            db.Comments.AddRange(new List<Comments> {
            new Comments{
                FromUserId=1,
                MsgId=2,
                Comment="天气挺好的"
            }

            });

            db.SaveChanges();
            base.Seed(db);
        }
    }
}