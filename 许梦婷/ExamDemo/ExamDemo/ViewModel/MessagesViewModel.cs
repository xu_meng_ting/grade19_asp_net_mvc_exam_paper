﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.ViewModel
{
    public class MessagesViewModel
    {
        public int Id { get; set; }
        public String Content { get; set; }
        public IEnumerable<CommentViewmodel> CommentViewmodels { get; set; }
    }
}