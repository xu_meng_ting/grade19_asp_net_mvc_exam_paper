﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.ViewModel
{
    public class CommentViewmodel
    {
        public string FromUsername { get; set; }
        public string ToUsername { get; set; }
        public string Comment { get; set; }
    }
}