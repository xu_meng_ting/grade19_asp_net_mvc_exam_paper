﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.ViewModel
{
    public class SayViewModel
    {
        public string Username { get; set; }
        public IEnumerable<MessagesViewModel> MessageViewModels { get; set; }
    }
}